package net.izfe.g240.wfiguiarapidaspringmvcweb.rest;

import net.izfe.g240.wfiguiarapidacorelib.beans.Cuenta;
import net.izfe.g240.wfiguiarapidacorelib.beans.CuentasFilter;
import net.izfe.g240.wfiguiarapidacorelib.facades.CuentasFacade;
import net.izfe.g240.wfiguiarapidaspringmvcweb.beans.CuentasWrapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;


@Controller
@RequestMapping("/api/cuentas")
public class RestCuentaController {

  @Autowired
  private CuentasFacade cuentasFacade;

  @RequestMapping(value = "{id}", produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public Cuenta getById(@PathVariable final String id) {
    return this.cuentasFacade.leerCuenta(id);
  }

  @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public CuentasWrapper all() {
    final Pageable pageable = new PageRequest(0, 10);
    final Page<Cuenta> cuentas = this.cuentasFacade.leerCuentas(new CuentasFilter(), pageable);
    final CuentasWrapper wrapper = new CuentasWrapper(cuentas.getContent());
    return wrapper;
  }

  @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
  @ResponseStatus(HttpStatus.OK)
  public void guardarCuentas(@RequestBody final CuentasWrapper cuentasWrapper) {
    for (final Cuenta cuenta : cuentasWrapper.getCuentas()) {
      this.cuentasFacade.crearCuenta(cuenta);
    }
  }
}
