package net.izfe.g240.wfiguiarapidaspringmvcweb.validators;

import net.izfe.g240.wfiguiarapidacorelib.beans.Cuenta;
import net.izfe.g240.wfiguiarapidacorelib.facades.CuentasFacade;
import net.izfe.g240.wfiguiarapidaspringmvcweb.beans.CuentaWizard;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


/**
 * Esto es un ejemplo de validador para el wizard. En realidad, es una copia del validador
 * {@link UsuarioExistenteValidator}. En una aplicaci�n real para evitar copiar el validador, habr�a que crear un
 * interface que implementen ambos beans. El interface tendr�a el m�todo getIdUsuario y el m�todo support del validador
 * comprobar�a si implementa dicha interface o no.
 */
@Component
public class UsuarioWizardExistenteValidator implements Validator {

  @Autowired
  private CuentasFacade cuentasFacade;

  @Override
  public boolean supports(final Class<?> clazz) {
    return CuentaWizard.class.equals(clazz);
  }

  @Override
  public void validate(final Object object, final Errors errors) {
    final String idUsuario = ((CuentaWizard) object).getIdUsuario();

    if (StringUtils.hasText(idUsuario)) {
      final Cuenta cuenta = this.cuentasFacade.leerCuenta(idUsuario);

      if (cuenta != null) {
        errors.rejectValue("idUsuario", "errors.idusuario.existente");
      }
    }
  }

}
