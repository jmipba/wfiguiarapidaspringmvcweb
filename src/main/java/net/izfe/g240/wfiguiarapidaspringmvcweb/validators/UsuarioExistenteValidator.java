package net.izfe.g240.wfiguiarapidaspringmvcweb.validators;

import net.izfe.g240.wfiguiarapidacorelib.beans.Cuenta;
import net.izfe.g240.wfiguiarapidacorelib.facades.CuentasFacade;
import net.izfe.g240.wfiguiarapidaspringmvcweb.beans.CuentaForm;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;


@Component
public class UsuarioExistenteValidator implements Validator {

  @Autowired
  private CuentasFacade cuentasFacade;

  @Override
  public boolean supports(final Class<?> clazz) {
    return CuentaForm.class.equals(clazz);
  }

  @Override
  public void validate(final Object object, final Errors errors) {
    final String idUsuario = ((CuentaForm) object).getCuenta().getIdUsuario();

    if (StringUtils.hasText(idUsuario)) {
      final Cuenta cuenta = this.cuentasFacade.leerCuenta(idUsuario);

      if (cuenta != null) {
        errors.rejectValue("cuenta.idUsuario", "errors.idusuario.existente");
      }
    }
  }

}
