/*
 * Copyright (c) 2018, Gipuzkoako Foru Aldundia
 * Eskubide guztiak erreserbatuta / All rights reserved
 */
package net.izfe.g240.wfiguiarapidaspringmvcweb.beans;

/**
 * @author Irazu
 */
public class BuscarCuentaPorIdForm {

  private String id;

  public String getId() {
    return this.id;
  }

  public void setId(final String id) {
    this.id = id;
  }

}
