package net.izfe.g240.wfiguiarapidaspringmvcweb.beans;

import javax.validation.constraints.NotNull;


public class DireccionCuenta {

  @NotNull
  private Integer comunidadId;

  @NotNull
  private Integer provinciaId;

  @NotNull
  private Integer municipioId;

  public Integer getComunidadId() {
    return this.comunidadId;
  }

  public void setComunidadId(final Integer comunidadId) {
    this.comunidadId = comunidadId;
  }

  public Integer getProvinciaId() {
    return this.provinciaId;
  }

  public void setProvinciaId(final Integer provinciaId) {
    this.provinciaId = provinciaId;
  }

  public Integer getMunicipioId() {
    return this.municipioId;
  }

  public void setMunicipioId(final Integer municipioId) {
    this.municipioId = municipioId;
  }

}
