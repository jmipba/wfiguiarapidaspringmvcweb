package net.izfe.g240.wfiguiarapidaspringmvcweb.beans;

import java.util.List;

import net.izfe.g240.wfiframeworkizfelib.validator.constraints.NotEmpty;


public class Acciones {

  @NotEmpty
  private List<String> selectedIds;

  /**
   * @return the selectedIds
   */
  public List<String> getSelectedIds() {
    return this.selectedIds;
  }

  /**
   * @param selectedIds
   *          the selectedIds to set
   */
  public void setSelectedIds(final List<String> selectedIds) {
    this.selectedIds = selectedIds;
  }

}
