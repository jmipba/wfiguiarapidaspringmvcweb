package net.izfe.g240.wfiguiarapidaspringmvcweb.controllers;

import java.util.Date;
import java.util.Locale;

import net.izfe.g240.wfiframeworkizfelib.springmvc.select.SelectOptions;
import net.izfe.g240.wfiguiarapidacorelib.beans.Cuenta;
import net.izfe.g240.wfiguiarapidacorelib.beans.CuentasFilter;
import net.izfe.g240.wfiguiarapidacorelib.beans.Idioma;
import net.izfe.g240.wfiguiarapidacorelib.facades.CuentasFacade;
import net.izfe.g240.wfiguiarapidaspringmvcweb.beans.Acciones;
import net.izfe.g240.wfiguiarapidaspringmvcweb.beans.CuentaForm;
import net.izfe.g240.wfiguiarapidaspringmvcweb.conversores.ConversorPrefijo;
import net.izfe.g240.wfiguiarapidaspringmvcweb.validators.ConfirmacionPasswordValidator;
import net.izfe.g240.wfiguiarapidaspringmvcweb.validators.UsuarioExistenteValidator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import net.sf.jasperreports.engine.JREmptyDataSource;


@RequestMapping("/cuentas")
@Controller
public class CuentasController {

  private static final Pageable FIRST_PAGE = new PageRequest(0, 10, Sort.Direction.ASC, "idUsuario");

  @Autowired
  private UsuarioExistenteValidator usuarioExistenteValidator;

  @Autowired
  private CuentasFacade cuentasFacade;

  @Autowired
  private MessageSource messageSource;

  @InitBinder("cuentaForm")
  public void initBinder(final WebDataBinder binder) {
    binder.registerCustomEditor(Integer.class, "cuenta.telefono", new ConversorPrefijo());
    binder.addValidators(new ConfirmacionPasswordValidator(), this.usuarioExistenteValidator);
  }

  @ModelAttribute
  public Idioma[] getIdiomas() {
    return Idioma.values();
  }

  @ModelAttribute
  public CuentasFilter cuentasFilter() {
    return new CuentasFilter();
  }

  @RequestMapping("/{idUsuario}")
  public String verCuenta(@PathVariable final String idUsuario, final Model model) {
    final Cuenta cuenta = this.cuentasFacade.leerCuenta(idUsuario);
    model.addAttribute(cuenta);

    return "cuentas/verCuenta";
  }

  @RequestMapping("/new")
  public String crearCuentaForm(final Model model) {
    model.addAttribute(new CuentaForm());

    return "cuentas/nuevaCuenta";
  }

  @RequestMapping(value = "/new", method = RequestMethod.POST)
  public String crearCuenta(@Validated @ModelAttribute final CuentaForm cuentaForm, final BindingResult bindingResult,
      final RedirectAttributes redirectAttributes, final SessionStatus sessionStatus) {

    if (bindingResult.hasErrors()) {
      return "cuentas/nuevaCuenta";
    }

    this.cuentasFacade.crearCuenta(cuentaForm.getCuenta());

    sessionStatus.setComplete();
    redirectAttributes.addFlashAttribute("successMessage", "cuentaCreadaMessage");

    return "redirect:/cuentas";
  }

  @RequestMapping()
  public String listado(@ModelAttribute final CuentasFilter cuentasFilter,
      @PageableDefault(sort = "idUsuario") final Pageable pageable, final Model model) {
    model.addAttribute("cuentas", this.cuentasFacade.leerCuentas(cuentasFilter, pageable));
    model.addAttribute(new Acciones());

    return "cuentas/listadoCuentas";
  }

  @RequestMapping(headers = { "X-Requested-With" })
  public String soloTabla(@ModelAttribute final CuentasFilter cuentasFilter, final Pageable pageable,
      final Model model) {
    this.listado(cuentasFilter, pageable, model);
    return "cuentas/tablaCuentas.jsp";
  }

  @RequestMapping(value = "/delete", method = RequestMethod.POST)
  public String delete(@Validated final Acciones acciones, final BindingResult bindingResult,
      final RedirectAttributes redirectAttributes, final Model model) {

    if (bindingResult.hasErrors()) {
      model.addAttribute("errorMessage", "errors.delete.emptyids");
      return this.listado(new CuentasFilter(), FIRST_PAGE, model);
    }

    this.cuentasFacade.eliminarCuentas(acciones.getSelectedIds());
    redirectAttributes.addFlashAttribute("successMessage", "cuentaEliminadaMessage");

    return "redirect:/cuentas";
  }

  @RequestMapping("/pdf")
  public String crearInformePdf(final Pageable pageable, final Locale locale, final Model model) {
    final Page<Cuenta> listaCuentas = this.cuentasFacade.leerCuentas(new CuentasFilter(), pageable);

    model.addAttribute("datasource", listaCuentas);
    model.addAttribute("fecha", new Date());
    model.addAttribute("DATE_PATTERN", this.messageSource.getMessage("format.date.short", null, locale));
    model.addAttribute("SUBREPORT_DATASOURCE", new JREmptyDataSource());

    return "pdfReport";
  }

  @RequestMapping(value = "/idiomas", produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public SelectOptions getIdiomasAjax(@RequestParam final String q, final Locale locale) {
    final Idioma[] idiomas = Idioma.values();

    final SelectOptions selectOptions = new SelectOptions();
    for (final Idioma idioma : idiomas) {
      selectOptions.addOption(idioma.name(), idioma.getMessage(locale));
    }

    return selectOptions;
  }

}
