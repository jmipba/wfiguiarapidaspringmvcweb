package net.izfe.g240.wfiguiarapidaspringmvcweb.controllers;

import java.util.List;

import net.izfe.g240.wfiframeworkizfelib.springmvc.select.SelectOptions;
import net.izfe.g240.wfiguiarapidacorelib.beans.Municipio;
import net.izfe.g240.wfiguiarapidacorelib.beans.Provincia;
import net.izfe.g240.wfiguiarapidacorelib.facades.MunicipiosFacade;
import net.izfe.g240.wfiguiarapidaspringmvcweb.beans.DireccionCuenta;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


@RequestMapping("/selectores-enlazados")
@Controller
public class SelectoresEnlazadosController {

  @Autowired
  private MunicipiosFacade municipiosFacade;

  /**
   * Se muestra el formulario con el ejemplo de selectores enlazados. Se mete en el modelo las comunidades para el
   * formulario, y se deja las provincias y municipios para que se carguen con AJAX
   *
   * @param model
   * @return
   */
  @RequestMapping()
  public String getSelectEnlazados(final Model model) {
    model.addAttribute(new DireccionCuenta());
    model.addAttribute("comunidadList", this.municipiosFacade.findAllComunidades());

    return "selectEnlazados";
  }

  /**
   * Método para obtener las provincias por AJAX
   *
   * @param id
   * @param q
   * @return
   */
  @RequestMapping(value = "provincias", produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public SelectOptions getProvincias(@RequestParam final int id, @RequestParam final String q) {
    final List<Provincia> provincias = this.municipiosFacade.findProvinciasByComunidadId(id);
    return new SelectOptions(provincias, "id", "nombre");
  }

  /**
   * Método para obtener los municipios por AJAX
   *
   * @param provinciaId
   * @param q
   * @return
   */
  @RequestMapping(value = "municipios", produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public SelectOptions getMunicipios(@RequestParam("provinciaId") final int provinciaId, @RequestParam final String q) {
    final List<Municipio> municipios = this.municipiosFacade.findMunicipiosByProvinciaId(provinciaId);

    return new SelectOptions(municipios, "id", "nombre");
  }

  /**
   * Se envía el formulario con la información obtenida en los selectores enlazados. Este método debería ejecutar una
   * lógica de negocio para, por ejemplo, actualizar la dirección de una cuenta. Pero al ser un método DEMO, de momento
   * no hace nada de eso
   *
   * @param direccionCuenta
   * @param bindingResult
   * @param redirectAttributes
   * @param model
   * @return
   */
  @RequestMapping(method = RequestMethod.POST)
  public String submit(@Validated @ModelAttribute final DireccionCuenta direccionCuenta,
      final BindingResult bindingResult, final RedirectAttributes redirectAttributes, final Model model) {

    if (bindingResult.hasErrors()) {
      model.addAttribute("comunidadList", this.municipiosFacade.findAllComunidades());
      return "selectEnlazados";
    }

    redirectAttributes.addFlashAttribute("successMessage", "selectoresEnlazados.success");
    redirectAttributes.addFlashAttribute("komunitatea",
        this.municipiosFacade.findComunidadById(direccionCuenta.getComunidadId()));
    redirectAttributes.addFlashAttribute("probintzia",
        this.municipiosFacade.findProvinciaById(direccionCuenta.getComunidadId(), direccionCuenta.getProvinciaId()));
    redirectAttributes.addFlashAttribute("udalerria",
        this.municipiosFacade.findMunicipioById(direccionCuenta.getProvinciaId(), direccionCuenta.getMunicipioId()));

    return "redirect:/selectores-enlazados";
  }

}
