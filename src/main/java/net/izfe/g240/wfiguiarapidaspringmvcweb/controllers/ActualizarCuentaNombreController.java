/*
 * Copyright (c) 2018, Gipuzkoako Foru Aldundia
 * Eskubide guztiak erreserbatuta / All rights reserved
 */
package net.izfe.g240.wfiguiarapidaspringmvcweb.controllers;

import java.util.ArrayList;
import java.util.List;

import net.izfe.g240.wfiguiarapidacorelib.beans.ActualizarCuentaNombre;
import net.izfe.g240.wfiguiarapidacorelib.beans.Cuenta;
import net.izfe.g240.wfiguiarapidacorelib.beans.CuentasFilter;
import net.izfe.g240.wfiguiarapidacorelib.facades.CuentasFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


/**
 * @author Irazu
 */
@Controller
@RequestMapping("/cuentas/actualizar-nombre")
public class ActualizarCuentaNombreController {

  @Autowired
  private CuentasFacade cuentasFacade;

  @RequestMapping
  public String formulario(@PageableDefault(sort = "idUsuario") final Pageable pageable, final Model model) {
    model.addAttribute("idsList", this.getListaIds(pageable));
    model.addAttribute(new ActualizarCuentaNombre());

    return "cuentas/actualizar-nombre";
  }

  @RequestMapping(method = RequestMethod.POST)
  public String formulario(@Validated @ModelAttribute final ActualizarCuentaNombre actualizarCuentaNombre,
      final BindingResult bindingResult, final RedirectAttributes redirectAttributes, final Model model,
      @PageableDefault(sort = "idUsuario") final Pageable pageable) {

    if (bindingResult.hasErrors()) {
      model.addAttribute("idsList", this.getListaIds(pageable));
      return "cuentas/actualizar-nombre";
    }

    this.cuentasFacade.solicitarCambiarNombre(actualizarCuentaNombre);

    redirectAttributes.addFlashAttribute("successMessage", "cuentaModificadaMessage");

    return "redirect:/cuentas";
  }

  private List<String> getListaIds(final Pageable pageable) {
    final Page<Cuenta> cuentas = this.cuentasFacade.leerCuentas(new CuentasFilter(), pageable);
    final List<String> idsList = new ArrayList<>();
    for (final Cuenta cuenta : cuentas) {
      idsList.add(cuenta.getIdUsuario());
    }
    return idsList;
  }
}
