/*
 * Copyright (c) 2018, Gipuzkoako Foru Aldundia
 * Eskubide guztiak erreserbatuta / All rights reserved
 */
package net.izfe.g240.wfiguiarapidaspringmvcweb.controllers;

import java.util.List;

import net.izfe.g240.wfiframeworkizfelib.paginacion.db.PaginacionHandlerIzfe;
import net.izfe.g240.wfiguiarapidacorelib.beans.Cuenta;
import net.izfe.g240.wfiguiarapidacorelib.facades.CuentasFacade;
import net.izfe.g240.wfiguiarapidaspringmvcweb.beans.Acciones;
import net.izfe.g240.wfiguiarapidaspringmvcweb.controllers.CuentasPaginacionClaves.CuentasPaginacionHandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;


/**
 * @author inigo
 */
@SessionAttributes(types = { CuentasPaginacionHandler.class })
@RequestMapping("/cuentas/claves")
@Controller
public class CuentasPaginacionClaves {

  @Autowired
  private CuentasFacade cuentasFacade;

  @RequestMapping
  public String listado(final Model model) {
    final PaginacionHandlerIzfe paginacionHandler = new CuentasPaginacionHandler();
    final List<Cuenta> cuentas = this.cuentasFacade.leerCuentas(paginacionHandler);
    paginacionHandler.actualizarPaginacion(cuentas);

    final Pageable pageable = new PageRequest(1, 10);
    final PageImpl<Cuenta> page = new PageImpl<>(cuentas, pageable, 30);

    model.addAttribute("cuentas", page);
    model.addAttribute(paginacionHandler);

    model.addAttribute(new Acciones());

    return "cuentas/listadoCuentas-no-tag";
  }

  @RequestMapping(params = { "page" }, headers = { "X-Requested-With" })
  public String soloTabla(@RequestParam final int page, @RequestParam final int pageSize,
      @ModelAttribute final CuentasPaginacionHandler paginacionHandler, final Model model) {

    paginacionHandler.irPaginaIndice(page);

    final List<Cuenta> cuentas = this.cuentasFacade.leerCuentas(paginacionHandler);
    paginacionHandler.actualizarPaginacion(cuentas);

    final Pageable pageable = new PageRequest(page, pageSize);
    final PageImpl<Cuenta> pageCuentas = new PageImpl<>(cuentas, pageable, 30);

    model.addAttribute("cuentas", pageCuentas);
    model.addAttribute(paginacionHandler);

    model.addAttribute(new Acciones());

    return "cuentas/listadoCuentas-no-tag.jsp";
  }

  public static class CuentasPaginacionHandler extends PaginacionHandlerIzfe {

    private static final long serialVersionUID = 1L;

    public CuentasPaginacionHandler() {
      this.setTamanoPagina(10);
      this.setCriterio("idUsuario", PaginacionHandlerIzfe.ORDEN_ASCENDENTE);
      this.irPaginaPrimera();
    }

  }

}
