package net.izfe.g240.wfiguiarapidaspringmvcweb.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class EncodingController {

	private static final Logger LOGGER = LoggerFactory.getLogger(EncodingController.class);

	@RequestMapping(value = "/encodingUriPathVariable/nombre/{nombre}", method = RequestMethod.GET)
	public String encodingPathVariable(@PathVariable String nombre, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		String aux = "Nombre: " + nombre + " Request Encoding:" + request.getCharacterEncoding()
				+ " Response Encoding: " + response.getCharacterEncoding();
		LOGGER.info(aux);
		model.addAttribute("datosCics", aux);
		return "cuentas/verEncoding";
	}

	@RequestMapping(value = "/encodingURIGet", method = RequestMethod.GET)
	public String encodingGet(String nombre, Model model, HttpServletRequest request, HttpServletResponse response) {

		String aux = "Nombre: " + nombre + " Request Encoding:" + request.getCharacterEncoding()
				+ " Response Encoding: " + response.getCharacterEncoding();
		LOGGER.info(aux);
		model.addAttribute("datosCics", aux);
		return "cuentas/verEncoding";
	}

	@RequestMapping(value = "/encodingURIGetISO", method = RequestMethod.GET)
	public String encodingGetISO(String nombre, Model model, HttpServletRequest request, HttpServletResponse response) {

		return "cuentas/verEncodingISO";
	}

	@RequestMapping(value = "/encodingURIGetUTF8", method = RequestMethod.GET)
	public String encodingGetUTF8(String nombre, Model model, HttpServletRequest request,
			HttpServletResponse response) {

		return "cuentas/verEncodingUTF8";
	}
}
