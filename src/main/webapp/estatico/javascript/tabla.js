/**
 * Funcion para obtener los elementos seleccionados del paginador con id indicado Para conocer el número de elementos,
 * usar getSelectedIds(pagId).length
 */
function getSelectedIds(paginationId) {
  return $('#' + paginationId + '_selectionForm input').map(function() {
    return this.value;
  });
}

/**
 * Metodo para actualizar el combo de seleccion de acciones del paginador con id indicado en funcion del numero de
 * elementos seleccionados y de los atributos min y max de cada accion
 */
function updateActionSelect(paginationId) {
  var num = getSelectedIds(paginationId).length;
  $('button.table-action-button').each(function(index) {
    if ((num < $(this).data("min")) || (($(this).data("max") != "0") && (num > $(this).data("max")))) {
      $(this).prop('disabled', true);
    } else {
      $(this).prop('disabled', false);
    }
  });

  // $('#' + paginationId + '_actionSelection').select2({
  // placeholder : $("#" + paginationId + "_actionSelection").data("placeholder")
  // });

  // $('select').select2({
  // minimumResultsForSearch : -1
  // });

  $('span.select2.select2-container.select2-container--default').hide();
}

function loadPagination(paginationId, url) {
  // Obtener elementos seleccionados
  var selectedIds = $('#' + paginationId + '_selectionForm input').map(function() {
    return this.value;
  }).get();

  // Valores de los formularios de acciones
  var hiddenActionForms = $('#' + paginationId + '_action_forms_container form');
  var actionSelects = hiddenActionForms.find("select");

  var success = function() {
    // Recrear bloque elementos seleccionados
    jQuery.each(selectedIds, function(i, val) {
      var template = '<input type="hidden" name="selectedIds" value="' + val + '"/>';
      $('#' + paginationId + '_selectionForm').append(template);
      // Marcar el elemento si existe en la pagina actual
      $('.table#' + paginationId + ' input:checkbox[name="selection"][value="' + val + '"]').prop('checked', true);
    });
    $('#' + paginationId).trigger({
      type : "checksChecked"
    });

    // Actualizar formularios de acciones, necesario para soporte HDIV
    var hiddenActionForms = $('#' + paginationId + '_action_forms_container form');
    var selects = hiddenActionForms.find("select");

    jQuery.each(selects, function(i, val) {
      var select = $(val);
      // Add previous values to the select
      var previousSelect = actionSelects.get(i);
      var previousOptions = $(previousSelect).find("option");
      // Añadir option previas
      jQuery.each(previousOptions, function(index, option) {
        // mirar si existe, si es así no añadir
        var id = $(option).data("id");
        var length = select.find("option[data-id='" + id + "']").length;
        if (length == 0) {
          select.append(option);
        }
      });

    });
    updateActionSelect(paginationId);
  };

  var target = $("#" + paginationId + "_shell");

  $.ajax({
    headers : {
      "Accept" : "text/html;type=ajax"
    },
    method : "GET",
    url : url,
    beforeSend : function() {
      showBusy();
    },
    success : function(data) {
      var content = data;
      target.hide().html(content).fadeIn(200, function() {
        success();
      });
    }

  }).always(function() {
    hideBusy();
  });
}

function showBusy() {

}
function hideBusy() {

}
function resetChild(childId) {
  $('#' + childId).val('');
}

function loadChild(parentId, childId, childUrl) {

  resetChild(childId);

  var value = $("#" + parentId).val();

  $("#" + childId)
      .select2(
          {
            dropdownParent : $('.modal'),
            minimumResultsForSearch : 2,
            containerCssClass : 'form-control',
            ajax : {
              url : childUrl + value,
              dataType : 'json',
              delay : 250,
              processResults : function(data, page) {
                return {
                  results : data
                };
              },
              cache : true,
              error : function(error) {
                console.log('Se ha producido un error de comunicación con el servidor');
                var msg = '<label class=\"error\" for=\"users\">Se ha producido un error de comunicación con el servidor</label>';
                $('div#error').html(msg);
              },
              escapeMarkup : function(markup) {
                return markup;
              },
            }
          });

}

function refreshPage() {

  // Se desseleccionan todos los elementos, tanto los de la pagina actual como
  // los del resto de paginas
  $('#declaraciones_table_zerga_selectionForm input').each(function() {
    $(this).remove();
  });
  $("input[name='selection']:checked").each(function() {
    $(this).prop('checked', false).trigger('change');
  });

  eval($('ul.pagination>li.active>a').attr('onclick').split(';')[0]);
}

function confirmarTableActionButton(actionId, tableId) {
  $(document).ready(function() {
    var ventanaModal = $("#modal-" + actionId);
    ventanaModal
    // Cuando pulsa en confirmar es cuando se realiza la acción
    .one('click', '#confirm-yes', function(e) {
      // Se quita la ventana modal
      ventanaModal.modal('hide');

      // Se realiza la acción
      ejecutarTableActionButton(actionId, tableId);
    });
  });

  buttonId = $('#' + actionId);
  // Se añade el evento on click
  buttonId.click(function(e) {
    // Se cancela la acción
    e.preventDefault();

    // Se detiene la propagación de eventos
    e.stopPropagation();

    // Se muestra el panel modal
    mostrarPanelConfirmacion(actionId, tableId);
  });
}

function mostrarPanelConfirmacion(actionId, tableId) {
  // Se crea la ventana modal a partir de su html
  var ventanaModal = $("#modal-" + actionId);

  // Se muestra el panel modal
  ventanaModal.modal('show');
}

function ejecutarTableActionButton(actionId, tableId) {
  // Form que contiene los datos a mandar al servidor
  // falta por seleccionar la selección de filas del usuario.
  var form = $("#form_formulario_" + actionId);

  // Eliminar el atributo 'selected' de los <options>
  var selectedOptions = $("option[data-id]");
  jQuery.each(selectedOptions, function(i, val) {
    $(val).removeAttr('selected');
  });

  var selectionIds = $("#" + tableId + "_selectionForm input[type=hidden]");
  jQuery.each(selectionIds, function(i, val) {
    var id = val.value;
    form.find("option[data-id='" + id + "']").attr('selected', "true");
  });

  // Mandar el formulario
  form.submit();
}

function ejecutarTableActionButtonSinModal(actionId, tableId) {
  var actionButton = $('#' + actionId);
  actionButton.click(function() {
    ejecutarTableActionButton(actionId, tableId);
  });
}

function selectAll() {
  $("input[name='selection']:not(:checked)").each(function() {
    // Se recupera la input
    var input = $(this);

    // se selecciona el input
    input.prop('checked', true).trigger('change');
  });
  $('#checkboxAll').prop('checked', true);
}

function selectNoOne() {
  $("input[name='selection']").each(function() {
    // Se recupera la input
    var input = $(this);

    // se deselecciona el input
    input.prop('checked', false).trigger('change');
  });
  $('#checkboxAll').prop('checked', false);
}
