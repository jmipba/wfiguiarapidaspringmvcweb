<!doctype html>
<%@ include file="taglibs.jsp" %>
<html lang="${springRequestContext.locale}">
	

<head>
    <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Guía Rápida</title>

    <spring:eval var="ibot_estaticos_url" htmlEscape="true" expression="'http://'+ T(net.izfe.g610.swsvariablesentorno.VariablesEntorno).getVariable('ruta_estaticos_ibot')" />
    
    <link rel="shortcut icon" type="image/x-icon" href="${ibot_estaticos_url}/img/favicon.ico">

    <!-- Modernizr (REQUIRED) -->
    <script src="${ibot_estaticos_url}/js/dependencies/modernizr.min.js"></script>

    <!-- Bootstrap Tour (OPTIONAL) -->
    <link rel="stylesheet" href="${ibot_estaticos_url}/css/plugins/bootstrap-tour/bootstrap-tour.min.css">

    <!-- DataTables (OPTIONAL) -->
    <link rel="stylesheet" href="${ibot_estaticos_url}/css/plugins/datatables/datatables.min.css">

    <!-- TreeGrid jQuery plugin (OPTIONAL) -->
    <link rel="stylesheet" href="${ibot_estaticos_url}/css/plugins/jquery-treegrid/jquery.treegrid.css">

    <!-- Select2 (OPTIONAL) -->
    <link rel="stylesheet" href="${ibot_estaticos_url}/css/plugins/select2/select2.min.css">
    <link rel="stylesheet" href="${ibot_estaticos_url}/css/plugins/select2/select2-bootstrap.min.css">

    <!-- jQuery (REQUIRED) -->
    <script src="${ibot_estaticos_url}/js/dependencies/jquery-1.11.2.min.js"></script>

    <!-- IZFE Bootstrap Theme (Ibot) (REQUIRED) -->
    <link rel="stylesheet" href="${ibot_estaticos_url}/css/ibot.min.css" data-ibot-theme>
    
    <!-- Paginated Table -->
    <script src="${pageContext.request.contextPath}/estatico/javascript/tabla.js"></script>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/estatico/css/tabla.css">
    
</head>

<body>
    <div id="wrapper">
        <a href="#content" class="sr-only" id="skip-to-content">Ir al contenido</a>
        
        <!-- Aquí va la cabecera -->
        <tiles:insertAttribute name="cabecera"/>
        
        <!-- Aquí va el menú -->
        <tiles:insertAttribute name="menu-sup"/>
        
        <div id="content">
            <section>
                <div class="container-fluid">
                    <!-- Aquí va el contenido -->
                    <tiles:insertAttribute name="cuerpo"/>
                </div>
            </section>
        </div>
        
        <!-- Aquí va el pie -->
        <tiles:insertAttribute name="pie"/>

    </div>
    
    <!-- IZFE Bootstrap Theme (Ibot) (REQUIRED) -->
    <script src="${ibot_estaticos_url}/js/ibot.min.js"></script>

    <!-- AutoNumeric (OPTIONAL) -->
    <script src="${ibot_estaticos_url}/js/plugins/autoNumeric/autoNumeric-min.js"></script>

    <!-- Bootstrap 3 Datepicker (required Moment) (OPTIONAL) -->
    <script src="${ibot_estaticos_url}/js/plugins/moment/moment.min.js"></script>
    <script src="${ibot_estaticos_url}/js/plugins/moment/locales_es_eu.min.js"></script>
    <script src="${ibot_estaticos_url}/js/plugins/bootstrap-datetimepicker/bootstrap-datetimepicker.min.js"></script>

    <!-- Bootstrap Tour (OPTIONAL) -->
    <script src="${ibot_estaticos_url}/js/plugins/bootstrap-tour/bootstrap-tour.min.js"></script>

    <!-- DataTables (OPTIONAL) -->
    <script src="${ibot_estaticos_url}/js/plugins/datatables/datatables.min.js"></script>

    <!-- MatchHeight (OPTIONAL) -->
    <script src="${ibot_estaticos_url}/js/plugins/jquery-match-height/jquery.matchHeight-min.js"></script>

    <!-- TreeGrid jQuery plugin (OPTIONAL) -->
    <script src="${ibot_estaticos_url}/js/plugins/jquery-treegrid/jquery.treegrid.min.js"></script>
    <script src="${ibot_estaticos_url}/js/plugins/jquery-treegrid/jquery.treegrid.bootstrap3.js"></script>

    <!-- Select2 (OPTIONAL) -->
    <script src="${ibot_estaticos_url}/js/plugins/select2/select2.full.min.js"></script>
    
    <!-- Mask plugin (OPTIONAL) -->
    <script src="${ibot_estaticos_url}/js/plugins/jquery-mask/jquery.mask.min.js"></script>

    <!-- Bootstrap FileStyle (OPTIONAL) -->
<!--     <script src="${ibot_estaticos_url}/js/plugins/bootstrap-filestyle/bootstrap-filestyle.min.js"></script> -->
    
</body>
  
  
</html>