<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="taglibs.jsp" %>

<c:if test="${fn:contains(requestScope['javax.servlet.forward.request_uri'], '/inicio')}">
  <c:set var="inicioActive" value="active" />
</c:if>
<c:if test="${fn:contains(requestScope['javax.servlet.forward.request_uri'], '/cuentas')}">
  <c:choose>
    <c:when test="${fn:contains(requestScope['javax.servlet.forward.request_uri'], '/cuentas/actualizar-nombre')}">
      <c:set var="asincronoActive" value="active" />
    </c:when>    
    <c:otherwise>
      <c:set var="gestionCuentasActive" value="active" />
    </c:otherwise>
  </c:choose>
</c:if>
<c:if test="${fn:contains(requestScope['javax.servlet.forward.request_uri'], '/selectores-enlazados')}">
  <c:set var="selectoresEnlazadosActive" value="active" />
</c:if>

<nav id="nav" class="navbar">
    <div class="container-fluid">
        <div class="navbar-collapse js-ibot-collapsable-by-navbar-toggle collapse">
            <ul class="nav navbar-nav navbar-left">
                <li class="${inicioActive}">
                    <spring:url value="/inicio" var="inicioIbotUrl" />
                    <a href="${inicioIbotUrl}"><span class="icon-home ibot-lg"></span>&nbsp;&nbsp;<spring:message code="Inicio"/></a>
                </li>
                <li class="dropdown ${gestionCuentasActive}" >
                    <spring:url value="/cuentas" var="url" />
                    <a href="${url}"  class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="100" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span>&nbsp;&nbsp;<spring:message code="GestionCuentas" /></a>
                    <ul class="dropdown-menu">
                        <li><a href="${url}"><spring:message code="listadoCuentas"/></a></li>
                        
                        <li role="separator" class="divider"></li>
                        
                        <spring:url var="url" value="/cuentas/claves"/>
                        <li><a href="${url}"><spring:message code="listadoCuentasPorClaves"/></a></li>
                        
                        <li role="separator" class="divider"></li>
                        
                        <spring:url var="url" value="/cuentas/new" />
                        <li><a href="${url}"><spring:message code="registrar" /></a></li>
                        
                        <li role="separator" class="divider"></li>
                        
                        <spring:url var="url" value="/cuentas/new/init" />
                        <li><a href="${url}"><spring:message code="registrar.wizard" /></a></li>
                    </ul>
                </li>
                <li class="${asincronoActive}">
                    <spring:url value="/cuentas/actualizar-nombre" var="url" />
                    <a href="${url}"><span class="glyphicon glyphicon-transfer"></span>&nbsp;&nbsp;<spring:message code="Asincrono"/></a>
                </li>
                <li class="${selectoresEnlazadosActive}">
                    <spring:url value="/selectores-enlazados" var="url" />
                    <a href="${url}"><span class="glyphicon glyphicon-magnet"></span>&nbsp;&nbsp;<spring:message code="selectoresEnlazados"/></a>
                </li>
            </ul>

        </div>
    </div>
</nav>
<!-- end #menu -->