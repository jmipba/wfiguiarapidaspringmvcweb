<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="../templates/taglibs.jsp" %>
<script src="${pageContext.request.contextPath}/estatico/javascript/guia-rapida.js"><jsp:text>&amp;nbsp;</jsp:text></script>

<h1 class="text-uppercase"><spring:message code="selectoresEnlazados"/></h1>

<c:if test="${successMessage != null}">
  <div class="alert alert-success alert-dismissible fade in">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span>
      </button>
      <spring:message code="${successMessage}" arguments="${komunitatea},${probintzia},${udalerria}" />
  </div>
</c:if>

<form:form action="" modelAttribute="direccionCuenta">
  <div class="row">
      <div class="col-sm-12">
          <div class="panel panel-default">
              <div class="panel-body">
                      <div class="form-horizontal">
                          <spring:bind path="comunidadId">
                              <div class="form-group ${status.error ? 'has-error' : ''}">
                                  <label class="col-sm-5 control-label" for="comunidadId">
                                      <spring:message code="selectoresEnlazados.comunidad"/><em>*</em>
                                  </label>
                                  <div class="col-sm-7">
                                      <form:select cssClass="form-control" id="comunidades" path="comunidadId" >
                                        <form:option value="" />
                                        <form:options items="${comunidadList}" itemLabel="nombre" itemValue="id"/>
                                      </form:select>
                                      <form:errors path="comunidadId" cssClass="help-block"/>
                                  </div>
                              </div>
                          </spring:bind>
                          <spring:bind path="provinciaId">
                              <div class="form-group ${status.error ? 'has-error' : ''}">
                                  <label class="col-sm-5 control-label" for="provinciaId">
                                      <spring:message code="selectoresEnlazados.provincia"/><em>*</em>
                                  </label>
                                  <div class="col-sm-7">
                                      <spring:url var="url" value="/selectores-enlazados/provincias">
                                        <spring:param name="_MODIFY_HDIV_STATE_" value="provinciaId" />
                                        <spring:param name="id" value="hdiv-values-ref:comunidadId" />
                                      </spring:url>
                                      <form:select cssClass="form-control" id="provincias" path="provinciaId" data-ajax-url="${url}" data-linked-select="true" />
                                      <form:errors path="provinciaId" cssClass="help-block"/>
                                  </div>
                              </div>
                          </spring:bind>
                          <spring:bind path="municipioId">
                              <div class="form-group ${status.error ? 'has-error' : ''}">
                                  <label class="col-sm-5 control-label" for="municipioId">
                                      <spring:message code="selectoresEnlazados.municipio"/><em>*</em>
                                  </label>
                                  <div class="col-sm-7">
                                      <spring:url var="url" value="/selectores-enlazados/municipios">
                                        <spring:param name="_MODIFY_HDIV_STATE_" value="municipioId" />
                                        <spring:param name="provinciaId" value="hdiv-values-ref:provinciaId" />
                                      </spring:url> 
                                      <form:select cssClass="form-control" id="municipios" path="municipioId" data-ajax-url="${url}"  data-linked-select="true" 
                                          data-parent-select="#provincias" data-parent-param-name="provinciaId" />
                                      <form:errors path="municipioId" cssClass="help-block"/>
                                  </div>
                              </div>
                          </spring:bind>
                      </div>
              </div>
          </div>
      </div>
  </div>
  <div class="row text-right">
      <div class="col-sm-12">
          <form:button class="btn btn-primary">
              <span class="glyphicon glyphicon-ok"></span>&nbsp;
              <spring:message code="ok"/>
          </form:button>
      </div>
  </div>
</form:form>
