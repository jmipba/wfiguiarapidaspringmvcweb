<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="../templates/taglibs.jsp" %>

<spring:url var="url" value="/cuentas"/>
<a href="${url}" class="text-center inicio img-thumbnail">
    <spring:eval var="ibot_estaticos_url" htmlEscape="true" expression="'http://'+ T(net.izfe.g610.swsvariablesentorno.VariablesEntorno).getVariable('ruta_estaticos_ibot')" />
    <img src="${ibot_estaticos_url}/img/teleoperador-00.png" class="img-rounded" alt="Responsive image" >
	<h3><spring:message code="GestionCuentas" /></h3>
</a>
<div style="clear: both;">&nbsp;</div>
