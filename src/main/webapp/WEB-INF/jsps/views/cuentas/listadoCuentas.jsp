<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="../../templates/taglibs.jsp" %>
<script src="${pageContext.request.contextPath}/estatico/javascript/tabla.js"><jsp:text>&amp;nbsp;</jsp:text></script>

<h1 class="text-uppercase"><spring:message code="Cuentas"/></h1>

<c:if test="${successMessage != null}">
  <div class="alert alert-success alert-dismissible fade in">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span>
      </button>
      <spring:message code="${successMessage}"/>
  </div>
</c:if>

<c:if test="${errorMessage != null}">
    <div class="alert alert-danger alert-dismissible fade in">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        <spring:message code="${errorMessage}"/>
    </div>
</c:if>

<div class="row">
    <div class="col-xs-12 col-sm-6 col-sm-push-6">
        <div class="ibot-pull-right-sm ibot-pull-right-md ibot-pull-right-lg">
            <p class="btn-group" role="group">
                <button type="button" class="btn btn-info ibot-margin-bottom" data-toggle="modal" data-target="#modal-filtro" ><span class="icon-filtrar"></span><spring:message code="filtrar" /></button>
                <spring:url value="/cuentas" var="url" />
                <c:if test="${not empty param.id}">
                  <a class="btn btn-info" href="${url}" role="button">
                      <span class="icon-cerrar ibot-lg pull-left"></span><span class="sr-only">Eliminar filtros</span>
                  </a>
                </c:if>
            </p>
        </div>
    </div>
</div>

<div id="modal-filtro" class="modal fade" tabindex="-1" role="dialog" data-backdrop="static" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <p class="modal-title text-primary"><spring:message code="filtrar" /></p>
            </div>
            <form:form action="" method="GET" modelAttribute="cuentasFilter">
                <div class="panel-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-4 control-label" for="id">
                                <spring:message code="usuario" />
                            </label>
                            <div class="col-sm-8">
                                <div class="row">
                                    <div class="col-sm-8">
                                        <form:input class="form-control select_modelo" path="id"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal"><spring:message code="accion.cancelar" /></button>
                    <button type="submit" class="btn btn-primary"><spring:message code="filtrar.buscar" /></button>
                </div>
            </form:form>
        </div>
    </div>
</div>

<jsp:include page="tablaCuentas.jsp" />
