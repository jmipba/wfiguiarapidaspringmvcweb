<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="../../templates/taglibs.jsp" %>

<h1 class="text-uppercase"><spring:message code="DetalleCuenta"/></h1>

<div id="content">
    <section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="well">
                                        <p class="text-primary"><strong><spring:message code="Cuentas"/></strong></p>
                                        <hr>
                                        <p class="text-warning"><strong><spring:message code="datosUsuario"/></strong></p>
                                        <p><spring:message code="cuenta.idUsuario"/>: <strong><c:out value="${cuenta.idUsuario}"/></strong></p>
                                        <p><spring:message code="cuenta.password"/>: <strong><c:out value="${cuenta.password}"/></strong></p>
                                        <hr>
                                        <p class="text-warning"><strong><spring:message code="informacionCuenta"/></strong></p>
                                        <p><spring:message code="cuenta.nombre"/>: <strong><c:out value="${cuenta.nombre}"/></strong></p>
                                        <p><spring:message code="cuenta.apellido"/>: <strong><c:out value="${cuenta.apellido}"/></strong></p>
                                        <p><spring:message code="cuenta.email"/>: <strong><c:out value="${cuenta.email}"/></strong></p>
                                        <p><spring:message code="cuenta.telefono"/>: <strong><c:out value="${cuenta.telefono}"/></strong></p>
                                        <spring:message code="format.date.short" var="format"/>
                                        <p><spring:message code="cuenta.fcreacion"/>: <strong><fmt:formatDate value="${cuenta.fcreacion}" pattern="${format}"/></strong></p>
                                        <hr>
                                        <p class="text-warning"><strong><spring:message code="informacionPerfil"/></strong></p>
                                        <p><spring:message code="idPerfil"/>: <strong><c:out value="${cuenta.perfil.idPerfil}"/></strong></p>
                                        <p><spring:message code="idioma"/>:
                                            <strong>
                                                <spring:eval expression="cuenta.perfil.idioma.getMessage(pageContext.response.locale)" />
                                            </strong>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="row text-right">
                                <div class="col-sm-12">
                                    <a href="#" onclick="history.back()" class="btn btn-default" role="button"><spring:message code="accion.atras" /></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

</div>