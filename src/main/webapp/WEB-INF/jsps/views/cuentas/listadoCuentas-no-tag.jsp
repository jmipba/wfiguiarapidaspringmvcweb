<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="../../templates/taglibs.jsp" %>

<h1 class="text-uppercase"><spring:message code="titulo.paginacionPorClave"/></h1>

<c:if test="${successMessage != null}">
  <div class="alert alert-success alert-dismissible fade in">
      <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">×</span>
      </button>
      <spring:message code="${successMessage}"/>
  </div>
</c:if>

<c:choose>
	<c:when test="${fn:length(cuentas.content) > 0}">
		<!-- COntent updated by Ajax -->
		<div id="table-responsive">
		<table summary='<spring:message code="listadoCuentasDec"/>.' id="tablaCuentas" class="table table-hover table-striped table-bordered">
			<caption><spring:message code="listadoCuentas"/></caption>
			<thead>
				<tr>
					<th><spring:message code="usuario"/></th>
					<th><spring:message code="cuenta.nombre"/></th>
					<th><spring:message code="cuenta.apellido"/></th>
					<th><spring:message code="cuenta.telefono"/></th>
					<th><spring:message code="cuenta.fcreacion"/></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="cuenta" items="${cuentas.content}">
					<tr>
						<td>
							<spring:url var="url" value="/cuentas/${cuenta.idUsuario}"/>
							<a href="${url}">
								<c:out value="${cuenta.idUsuario}"/>
							</a>
						</td>
						<td><c:out value="${cuenta.nombre}"/></td>
						<td><c:out value="${cuenta.apellido}"/></td>
						<td><c:out value="(+34) ${cuenta.telefono}"/></td>
						<td>
							<spring:message code="format.date.short" var="format"/>
							<fmt:formatDate value="${cuenta.fcreacion}" pattern="${format}"/>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		
		<div class="paginacion">
			<c:if test="${!cuentasPaginacionHandler.primeraPagina}">
                <spring:url var="url" value="/cuentas/claves">
                  <spring:param name="page" value="1"/>
                  <spring:param name="pageSize" value="10"/>
                </spring:url>
                <a id="prev" href="${url}">
                  <c:out value="1" />
                </a> | 
				<spring:url var="url" value="/cuentas/claves">
					<spring:param name="page" value="${cuentasPaginacionHandler.cursorDePagina - 1}"/>
					<spring:param name="pageSize" value="10"/>
				</spring:url>
				<a id="prev" href="${url}">
					<spring:message code="previo"/>
				</a>
			</c:if>
			<button type="button" class="btn btn-default">
				<c:out value="${cuentasPaginacionHandler.cursorDePagina}"/>
			</button>
			<c:if test="${!cuentasPaginacionHandler.ultimaPagina}">
				<spring:url var="url" value="/cuentas/claves">
					<spring:param name="page" value="${cuentasPaginacionHandler.cursorDePagina + 1}"/>
					<spring:param name="pageSize" value="10"/>
				</spring:url>
				<a id="next" href="${url}">
					<spring:message code="siguiente"/>
				</a>
			</c:if>
		</div>
		</div>
	
	</c:when>
	<c:otherwise>
		<spring:message code="listaVacia"/>
	</c:otherwise>
</c:choose>

<%-- <h3><spring:message code="Acciones"/></h3> --%>
<!-- <ul> -->
<!-- 	<li> -->
<%-- 		<spring:url var="url" value="/cuentas/pdf"/> --%>
<%-- 		<a href="${url}"> --%>
<%-- 			<spring:message code="DescargaPdf"/> --%>
<!-- 		</a> -->
<!-- 	</li> -->
<!-- 	<li> -->
<%-- 		<spring:url var="url" value="/api/cuentas"/> --%>
<%-- 		<a href="${url}"> --%>
<%-- 			<spring:message code="DescargaXml"/> --%>
<!-- 		</a> -->
<!-- 	</li> -->
<!-- </ul> -->

<script>
"use strict";
$( document ).ready(function() {
	init();
});
function init(){
	$("#next, #prev").click(function(e){
		e.stopPropagation();
		e.preventDefault();
		
		var url = $(this).attr("href");
		
		$.ajax(url)
		.done(function(data) {
			
			//Reemplazar tabla
			var html = $(data);
			var container = html.filter("#table-responsive");
			$("#table-responsive").replaceWith(container);
			
			init();// Volver a asociar los listeners
		})
		.fail(function() {
			console.error("error...");
		});
	});
	
}
</script>
