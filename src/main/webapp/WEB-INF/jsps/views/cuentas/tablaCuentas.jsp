<%@ page contentType="text/html; charset=UTF-8" %>
<%@ include file="../../templates/taglibs.jsp" %>
<script src="${pageContext.request.contextPath}/estatico/javascript/tabla.js"><jsp:text>&amp;nbsp;</jsp:text></script>

<izfe:table id="cuentas" baseURL="auto" data="${cuentas}">
    <izfe:rowId>${row.idUsuario}</izfe:rowId>
    <izfe:columns>
        <izfe:column titleKey="usuario" property="idUsuario" colgroup="34%" >
            <spring:url var="editUrl" value="/cuentas/${row.idUsuario}" />
            <a href="${editUrl}" ><c:out value="${row.idUsuario}" /></a>
        </izfe:column>
        <izfe:column titleKey="cuenta.nombre" property="nombre" colgroup="34%" />
        <izfe:column titleKey="cuenta.apellido" property="apellido" colgroup="10%" />
        <izfe:column titleKey="cuenta.telefono" property="telefono" colgroup="10%" />
        <izfe:column titleKey="cuenta.fcreacion" property="fcreacion" colgroup="10%" >
            <spring:message code="format.date.short" var="format"/>
            <fmt:formatDate value="${row.fcreacion}" pattern="${format}"/>
        </izfe:column>
    </izfe:columns>
    <izfe:actions>
        <izfe:action url="/cuentas/delete" id="eliminarButton" min="1" titleKey="accion.eliminar" iconClass="icon-eliminar" confirmationMessage="accion.eliminarConfirmacion" />
    </izfe:actions>
</izfe:table>
