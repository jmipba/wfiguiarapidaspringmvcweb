<%@ include file="../../../templates/taglibs.jsp" %>
<script src='<spring:url value="/estatico/lib/validate/jquery.validate.js"/>'></script>
<script src='<spring:url value="/estatico/javascript/validate/jquery.validate.extension.js"/>'></script>
<script src='<spring:url value="/estatico/lib/validate/additional-methods.js"/>'></script>
<script src='<spring:url value="/estatico/javascript/validate/localization/messages_es.js"/>'></script>

<h1 class="text-uppercase"><spring:message code="registrar.wizard"/></h1>

    <div class="ibot-progress-tracker">
        <ul>
            <li>
                <span class="ibot-progress-tracker__step ibot-progress-tracker__step--selected">
                    <span class="ibot-progress-tracker__step__number">1</span>
                <span class="ibot-progress-tracker__step__desc"><spring:message code="informacionCuenta"/></span>
                </span>
            </li>
            <li>
                <span class="ibot-progress-tracker__step">
                    <span class="ibot-progress-tracker__step__number">2</span>
                <span class="ibot-progress-tracker__step__desc"><spring:message code="datosUsuario"/></span>
                </span>
            </li>
            <li>
                <span class="ibot-progress-tracker__step">
                    <span class="ibot-progress-tracker__step__number">3</span>
                <span class="ibot-progress-tracker__step__desc"><spring:message code="informacionPerfil"/></span>
                </span>
            </li>
        </ul>
    </div>

            <form:form action="${pageContext.request.contextPath}/cuentas/new/informacionCuenta" method="POST" modelAttribute="cuentaWizard">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title"><spring:message code="informacionCuenta"/></h2>
                            </div>
                            <div class="panel-body">
                                    <div class="form-horizontal">
                                        <spring:bind path="nombre">
                                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                                <label class="col-sm-5 control-label" for="nombre">
                                                    <spring:message code="cuenta.nombre"/><em>*</em>
                                                </label>
                                                <div class="col-sm-7">
                                                    <form:input class="form-control" path="nombre" />
                                                    <form:errors path="nombre" cssClass="help-block"/>
                                                </div>
                                            </div>
                                        </spring:bind>
                                        <spring:bind path="apellido">
                                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                                <label class="col-sm-5 control-label" for="apellido">
                                                    <spring:message code="cuenta.apellido"/><em>*</em>
                                                </label>
                                                <div class="col-sm-7">
                                                    <form:input class="form-control" path="apellido" />
                                                    <form:errors path="apellido" cssClass="help-block"/>
                                                </div>
                                            </div>
                                        </spring:bind>
                                        <spring:bind path="telefono">
                                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                                <label class="col-sm-5 control-label" for="telefono">
                                                    <spring:message code="cuenta.telefono"/><em>*</em>
                                                </label>
                                                <div class="col-sm-7">
                                                    <div class="row">
                                                        <div class="col-sm-5 col-md-8 col-lg-6">
                                                            <form:input class="form-control" path="telefono" placeholder="987654321" />
                                                        </div>
                                                    </div>
                                                    <form:errors path="telefono" cssClass="help-block"/>
                                                </div>
                                            </div>
                                        </spring:bind>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row text-right">
                    <div class="col-sm-12">
                        <spring:url value="/cuentas" var="url" />
                        <a href="${url}" class="btn btn-default" role="button"><spring:message code="accion.cancelar" /></a>
                        <form:button class="btn btn-primary">
                            <spring:message code="siguiente"/>
                        </form:button>
                    </div>
                </div>
            </form:form>
