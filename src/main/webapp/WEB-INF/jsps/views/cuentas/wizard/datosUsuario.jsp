<%@ include file="../../../templates/taglibs.jsp" %>
<script src='<spring:url value="/estatico/lib/validate/jquery.validate.js"/>'></script>
<script src='<spring:url value="/estatico/javascript/validate/jquery.validate.extension.js"/>'></script>
<script src='<spring:url value="/estatico/lib/validate/additional-methods.js"/>'></script>
<script src='<spring:url value="/estatico/javascript/validate/localization/messages_es.js"/>'></script>

<h1 class="text-uppercase"><spring:message code="registrar.wizard"/></h1>

    <div class="ibot-progress-tracker">
        <ul>
            <li>
                <span class="ibot-progress-tracker__step ibot-progress-tracker__step--done">
                    <span class="ibot-progress-tracker__step__number">1</span>
                <span class="ibot-progress-tracker__step__desc"><spring:message code="informacionCuenta"/></span>
                </span>
            </li>
            <li>
                <span class="ibot-progress-tracker__step ibot-progress-tracker__step--selected">
                    <span class="ibot-progress-tracker__step__number">2</span>
                <span class="ibot-progress-tracker__step__desc"><spring:message code="datosUsuario"/></span>
                </span>
            </li>
            <li>
                <span class="ibot-progress-tracker__step">
                    <span class="ibot-progress-tracker__step__number">3</span>
                <span class="ibot-progress-tracker__step__desc"><spring:message code="informacionPerfil"/></span>
                </span>
            </li>
        </ul>
    </div>

            <form:form action="" method="POST" modelAttribute="cuentaWizard">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title"><spring:message code="datosUsuario"/></h2>
                            </div>
                            <div class="panel-body">
                                    <div class="form-horizontal">
                                        <spring:bind path="idUsuario">
                                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                                <label class="col-sm-5 control-label" for="idUsuario">
                                                    <spring:message code="cuenta.idUsuario"/><em>*</em>
                                                </label>
                                                <div class="col-sm-7">
                                                    <form:input class="form-control" path="idUsuario" />
                                                    <form:errors path="idUsuario" cssClass="help-block"/>
                                                </div>
                                            </div>
                                        </spring:bind>
                                        <spring:bind path="password">
                                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                                <label class="col-sm-5 control-label" for="password">
                                                    <spring:message code="cuenta.password"/><em>*</em>
                                                </label>
                                                <div class="col-sm-7">
                                                    <form:password class="form-control" path="password"/>
                                                    <form:errors path="password" cssClass="help-block"/>
                                                </div>
                                            </div>
                                        </spring:bind>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row text-right">
                    <div class="col-sm-12">
                        <spring:url value="/cuentas/new/informacionCuenta" var="url" />
                        <a href="${url}" class="btn btn-default" role="button"><spring:message code="anterior" /></a>
                        <form:button class="btn btn-primary">
                            <spring:message code="siguiente"/>
                        </form:button>
                    </div>
                </div>
            </form:form>
