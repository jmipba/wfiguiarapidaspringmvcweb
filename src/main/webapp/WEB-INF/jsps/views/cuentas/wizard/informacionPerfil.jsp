<%@ include file="../../../templates/taglibs.jsp" %>
<script src='<spring:url value="/estatico/lib/validate/jquery.validate.js"/>'></script>
<script src='<spring:url value="/estatico/javascript/validate/jquery.validate.extension.js"/>'></script>
<script src='<spring:url value="/estatico/lib/validate/additional-methods.js"/>'></script>
<script src='<spring:url value="/estatico/javascript/validate/localization/messages_es.js"/>'></script>

<h1 class="text-uppercase"><spring:message code="registrar.wizard"/></h1>

    <div class="ibot-progress-tracker">
        <ul>
            <li>
                <span class="ibot-progress-tracker__step ibot-progress-tracker__step--done">
                    <span class="ibot-progress-tracker__step__number">1</span>
                <span class="ibot-progress-tracker__step__desc"><spring:message code="informacionCuenta"/></span>
                </span>
            </li>
            <li>
                <span class="ibot-progress-tracker__step ibot-progress-tracker__step--done">
                    <span class="ibot-progress-tracker__step__number">2</span>
                <span class="ibot-progress-tracker__step__desc"><spring:message code="datosUsuario"/></span>
                </span>
            </li>
            <li>
                <span class="ibot-progress-tracker__step ibot-progress-tracker__step--selected">
                    <span class="ibot-progress-tracker__step__number">3</span>
                <span class="ibot-progress-tracker__step__desc"><spring:message code="informacionPerfil"/></span>
                </span>
            </li>
        </ul>
    </div>
    
            <form:form action="" method="POST" modelAttribute="cuentaWizard">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title"><spring:message code="informacionPerfil"/></h2>
                            </div>
                            <div class="panel-body">
                                    <div class="form-horizontal">
                                        <spring:bind path="email">
                                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                                <label class="col-sm-5 control-label" for="email">
                                                    <spring:message code="cuenta.email"/><em>*</em>
                                                </label>
                                                <div class="col-sm-7">
                                                    <form:input class="form-control" path="email" />
                                                    <form:errors path="email" cssClass="help-block"/>
                                                </div>
                                            </div>
                                        </spring:bind>
                                        <spring:bind path="fcreacion">
                                            <div class="form-group ${status.error ? 'has-error' : ''}">
                                                <label class="col-sm-5 control-label" for="fcreacion">
                                                    <spring:message code="cuenta.fcreacion"/><em>*</em>
                                                </label>
                                                <div class="col-sm-7">
                                                    <div class="input-group date js-datetimepicker" id="fcreacion">
                                                        <form:input class="form-control" path="fcreacion" placeholder="dd/mm/yyyy" />
                                                        <span class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </span>
                                                    </div>
                                                    <script type="text/javascript">
                                                        $(function () {
                                                            $('#fcreacion').datetimepicker({
                                                              locale: '${pageContext.response.locale}',
                                                              format: 'DD/MM/YYYY'
                                                            });
                                                        });
                                                    </script>
                                                    <form:errors path="fcreacion" cssClass="help-block"/>
                                                </div>
                                            </div>
                                        </spring:bind>
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row text-right">
                    <div class="col-sm-12">
                        <spring:url value="/cuentas/new/datosUsuario" var="url" />
                        <a href="${url}" class="btn btn-default" role="button"><spring:message code="anterior" /></a>
                        <form:button class="btn btn-primary">
                            <span class="icon-guardar"></span>&nbsp;
                            <spring:message code="CrearCuenta"/>
                        </form:button>
                    </div>
                </div>
            </form:form>
