<%@ page pageEncoding="ISO-8859-1" %>
<%@ include file="../../templates/taglibs.jsp" %>
<script src='<spring:url value="/estatico/lib/validate/jquery.validate.js"/>'></script>
<script src='<spring:url value="/estatico/javascript/validate/jquery.validate.extension.js"/>'></script>
<script src='<spring:url value="/estatico/lib/validate/additional-methods.js"/>'></script>
<script src='<spring:url value="/estatico/javascript/validate/localization/messages_es.js"/>'></script>
<script src="${pageContext.request.contextPath}/estatico/javascript/guia-rapida.js"></script>

<h1 class="text-uppercase"><spring:message code="registrar"/></h1>

          <form:form action="" method="POST" modelAttribute="cuentaForm">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title"><spring:message code="Cuentas"/></h2>
                        </div>
                        <div class="panel-body">
                            <div class="well">
                                <p class="text-primary"><spring:message code="datosUsuario"/></p>
                                <div class="form-horizontal">
                                    <spring:bind path="cuenta.idUsuario">
                                      <div class="form-group ${status.error ? 'has-error' : ''}">
                                          <label class="col-sm-5 control-label" for="cuenta.idUsuario">
                                              <spring:message code="cuenta.idUsuario"/><em>*</em>
                                          </label>
                                          <div class="col-sm-7">
                                              <form:input class="form-control" path="cuenta.idUsuario" />
                                              <form:errors path="cuenta.idUsuario" cssClass="help-block"/>
                                          </div>
                                      </div>
                                    </spring:bind>
                                    <spring:bind path="cuenta.password">
                                        <div class="form-group ${status.error ? 'has-error' : ''}">
                                            <label class="col-sm-5 control-label" for="cuenta.password">
                                                <spring:message code="cuenta.password"/><em>*</em>
                                            </label>
                                            <div class="col-sm-7">
                                                <form:password class="form-control" path="cuenta.password" />
    				                            <form:errors path="cuenta.password" cssClass="help-block"/>
                                            </div>
                                        </div>
                                    </spring:bind>
                                    <spring:bind path="repetirPassword">
                                        <div class="form-group ${status.error ? 'has-error' : ''}">
                                            <label class="col-sm-5 control-label" for="repetirPassword">
                                                <spring:message code="repetirPassword"/><em>*</em>
                                            </label>
                                            <div class="col-sm-7">
                                        		<form:password class="form-control" path="repetirPassword" />
                                        		<form:errors path="repetirPassword" cssClass="help-block"/>
                                            </div>
                                        </div>
                                    </spring:bind>
                                </div>
                            </div>
                            <div class="well">
                                <p class="text-primary"><spring:message code="informacionCuenta"/></p>
                                <div class="form-horizontal">
                                    <spring:bind path="cuenta.nombre">
                                        <div class="form-group ${status.error ? 'has-error' : ''}">
                                            <label class="col-sm-5 control-label" for="cuenta.nombre">
                                                <spring:message code="cuenta.nombre"/><em>*</em>
                                            </label>
                                            <div class="col-sm-7">
                                                <form:input class="form-control" path="cuenta.nombre" />
    				                            <form:errors path="cuenta.nombre" cssClass="help-block"/>
                                            </div>
                                        </div>
                                    </spring:bind>
                                    <spring:bind path="cuenta.apellido">
                                        <div class="form-group ${status.error ? 'has-error' : ''}">
                                            <label class="col-sm-5 control-label" for="cuenta.apellido">
                                                <spring:message code="cuenta.apellido"/><em>*</em>
                                            </label>
                                            <div class="col-sm-7">
                                                <form:input class="form-control" path="cuenta.apellido" />
    				                            <form:errors path="cuenta.apellido" cssClass="help-block"/>
                                            </div>
                                        </div>
                                    </spring:bind>
                                    <spring:bind path="cuenta.email">
                                        <div class="form-group ${status.error ? 'has-error' : ''}">
                                            <label class="col-sm-5 control-label" for="cuenta.email">
                                                <spring:message code="cuenta.email"/><em>*</em>
                                            </label>
                                            <div class="col-sm-7">
                                                <form:input class="form-control" path="cuenta.email" />
    				                            <form:errors path="cuenta.email" cssClass="help-block"/>
                                            </div>
                                        </div>
                                    </spring:bind>
                                    <spring:bind path="cuenta.telefono">
                                        <div class="form-group ${status.error ? 'has-error' : ''}">
                                            <label class="col-sm-5 control-label" for="cuenta.telefono">
                                                <spring:message code="cuenta.telefono"/><em>*</em>
                                            </label>
                                            <div class="col-sm-7">
                                                <div class="row">
                                                    <div class="col-sm-5 col-md-8 col-lg-6">
                                                        <form:input class="form-control" path="cuenta.telefono" placeholder="987654321" />
                                                    </div>
                                                </div>
    				                            <form:errors path="cuenta.telefono" cssClass="help-block"/>
                                            </div>
                                        </div>
                                    </spring:bind>
                                    <spring:bind path="cuenta.fcreacion">
                                        <div class="form-group ${status.error ? 'has-error' : ''}">
                                            <label class="col-sm-5 control-label" for="cuenta.fcreacion">
                                                <spring:message code="cuenta.fcreacion"/><em>*</em>
                                            </label>
                                            <div class="col-sm-7">
                                                <div class="input-group date js-datetimepicker" id="fcreacion">
                                                    <form:input class="form-control" path="cuenta.fcreacion" placeholder="dd/mm/yyyy" />
                                                    <span class="input-group-addon">
                                                        <span class="icon-calendario ibot-lg pull-left"></span>
                                                    </span>
                                                </div>
                                                <script type="text/javascript">
                                                    $(function () {
                                                        $('#fcreacion').datetimepicker({
                                                          locale: '${pageContext.response.locale}',
                                                          format: 'DD/MM/YYYY'
                                                        });
                                                    });
                                                </script>
                                                <form:errors path="cuenta.fcreacion" cssClass="help-block"/>
                                            </div>
                                        </div>
                                    </spring:bind>
                                </div>
                            </div>
                            <div class="well">
                                <p class="text-primary"><spring:message code="informacionPerfilAjax"/></p>
                                <div class="form-horizontal">
                                    <div class="form-group">
                                        <label class="col-sm-5 control-label" for="idioma">
                                            <spring:message code="idioma"/><em>*</em>
                                        </label>
                                        <div class="col-sm-7">
                                            <spring:url value="/cuentas/idiomas" var="url">
                            					<spring:param name="_MODIFY_HDIV_STATE_" value="cuenta.perfil.idioma" />
                            				</spring:url>
                            				<form:select class="form-control select_modelo" style="width: 100%" path="cuenta.perfil.idioma" id="idioma" data-ajax-url="${url}" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row text-right">
                <div class="col-sm-12">
                    <spring:url value="/cuentas" var="url" />
                    <a href="${url}" class="btn btn-default" role="button"><spring:message code="accion.cancelar" /></a>
                    <form:button class="btn btn-primary">
                        <span class="icon-guardar"></span>&nbsp;
                        <spring:message code="CrearCuenta"/>
                    </form:button>
                </div>
            </div>
          </form:form>
