<%@ page pageEncoding="ISO-8859-1" %>
<%@ include file="../../templates/taglibs.jsp" %>
<script src='<spring:url value="/estatico/lib/validate/jquery.validate.js"/>'></script>
<script src='<spring:url value="/estatico/javascript/validate/jquery.validate.extension.js"/>'></script>
<script src='<spring:url value="/estatico/lib/validate/additional-methods.js"/>'></script>
<script src='<spring:url value="/estatico/javascript/validate/localization/messages_es.js"/>'></script>

<h1 class="text-uppercase"><spring:message code="Asincrono"/></h1>

<div id="content">
    <section>
        <div class="container-fluid">
          <form:form action="" method="POST" modelAttribute="actualizarCuentaNombre">
            <div class="row">
                <div class="col-sm-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title"><spring:message code="ActualizarNombreCuenta"/></h2>
                        </div>
                        <div class="panel-body">
                            <div class="well">
                                <p class="text-primary"><spring:message code="datosUsuario"/></p>
                                <div class="form-horizontal">
                                    <spring:bind path="id">
                                      <div class="form-group ${status.error ? 'has-error' : ''}">
                                          <label class="col-sm-5 control-label" for="id">
                                              <spring:message code="cuenta.idUsuario"/><em>*</em>
                                          </label>
                                          <div class="col-sm-7">
                                              <form:input class="form-control" path="id" />
                                              <form:errors path="id" cssClass="help-block"/>
                                          </div>
                                      </div>
                                    </spring:bind>
                                    <spring:bind path="nombre">
                                        <div class="form-group ${status.error ? 'has-error' : ''}">
                                            <label class="col-sm-5 control-label" for="nombre">
                                                <spring:message code="cuenta.nombre"/><em>*</em>
                                            </label>
                                            <div class="col-sm-7">
                                                <form:input class="form-control" path="nombre" />
    				                            <form:errors path="nombre" cssClass="help-block"/>
                                            </div>
                                        </div>
                                    </spring:bind>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row text-right">
                <div class="col-sm-12">
                    <spring:url value="/cuentas" var="url" />
                    <a href="${url}" class="btn btn-default" role="button"><spring:message code="accion.cancelar" /></a>
                    <form:button class="btn btn-primary">
                        <span class="icon-guardar"></span>&nbsp;
                        <spring:message code="CambiarNombre"/>
                    </form:button>
                </div>
            </div>
          </form:form>
        </div>
    </section>
</div>
