package net.izfe.g240.wfiguiarapidaspringmvcweb.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import net.izfe.g240.wfiguiarapidacorelib.beans.Cuenta;
import net.izfe.g240.wfiguiarapidacorelib.beans.Idioma;
import net.izfe.g240.wfiguiarapidacorelib.beans.Perfil;
import net.izfe.g240.wfiguiarapidaspringmvcweb.beans.CuentasWrapper;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;


public class RestCuentaClient {

  private static final int LIST_SIZE = 3;

  private static final Logger LOGGER = LoggerFactory.getLogger(RestCuentaClient.class);

  public static void main(final String[] args) throws Exception {

    // Llamar al servicio REST con Java puro.
    consumoREST();

    // Llamar al servicio REST con las utilidades de Spring
    consumoConSpring();
  }

  /**
   * Ejemplo de consumo de un servicio REST usando clases de la propia JDK.
   */
  private static void consumoREST() {

    try {

      final URL restServiceURL = new URL(
          "http://localhost:9080/WAS/HACI/WFIGuiaRapidaSpringMVCWEB/api/cuentas/prueba1");

      final HttpURLConnection httpConnection = (HttpURLConnection) restServiceURL.openConnection();
      httpConnection.setRequestMethod("GET");
      httpConnection.setRequestProperty("Accept", "application/xml");

      if (httpConnection.getResponseCode() != 200) {
        LOGGER.info("REST: Error code: {}", httpConnection.getResponseCode());
      }

      final BufferedReader responseBuffer = new BufferedReader(new InputStreamReader(httpConnection.getInputStream()));

      String output;
      final StringBuilder result = new StringBuilder();

      while ((output = responseBuffer.readLine()) != null) {
        result.append(output);
      }

      LOGGER.info("REST: Result: {}", result);

      httpConnection.disconnect();

    } catch (final MalformedURLException e) {
      e.printStackTrace();
    } catch (final IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Ejemplo de consumo de un servicio REST usando la clase de Spring RestTemplate para facilitar la llamada y consumo
   * del resultado
   * 
   * @throws URISyntaxException
   */
  private static void consumoConSpring() throws URISyntaxException {

    // Cargar ApplicationContext para el cliente REST
    final ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
        "/net/izfe/g240/wfiguiarapidaspringmvcweb/rest/applicationContext-rest-client.xml");

    // Obtener instancia de RestTemplate
    final RestTemplate template = context.getBean(RestTemplate.class);

    final Cuenta cuenta = template.getForObject(
        "http://localhost:9080/WAS/HACI/WFIGuiaRapidaSpringMVCWEB/api/cuentas/{cuentaId}", Cuenta.class,
        "prueba_nulos");

    LOGGER.info("REST: Nombre de cuenta: {}", cuenta.getNombre());

    final CuentasWrapper cuentas = template
        .getForObject("http://localhost:9080/WAS/HACI/WFIGuiaRapidaSpringMVCWEB/api/cuentas", CuentasWrapper.class);

    LOGGER.info("REST: Numero de cuentas: {}", cuentas.getCuentas().size());

    final URI uri = new URI("http://localhost:9080/WAS/HACI/WFIGuiaRapidaSpringMVCWEB/api/cuentas");
    final CuentasWrapper cuentasWrapper = new CuentasWrapper(crearCuentaList());
    final ResponseEntity<?> response = template.postForEntity(uri, cuentasWrapper, ResponseEntity.class);

    LOGGER.info("REST: Response status code: " + response.getStatusCode());

    context.close();
  }

  private static List<Cuenta> crearCuentaList() {
    final List<Cuenta> cuentaList = new ArrayList<>();
    final long time = new Date().getTime();
    for (int i = 0; i < LIST_SIZE; i++) {
      final Cuenta cuenta = new Cuenta();
      cuenta.setApellido("apellido_" + time + i);
      cuenta.setEmail("email_" + time + i);
      cuenta.setFcreacion(new Date());
      cuenta.setIdUsuario("id_" + time + i);
      cuenta.setNombre("nombre_" + time + i);
      cuenta.setPassword("password_" + time + i);
      cuenta.setTelefono(943112233);

      final Perfil perfil = new Perfil();
      perfil.setIdioma(Idioma.INGLES);
      cuenta.setPerfil(perfil);

      cuentaList.add(cuenta);
    }

    return cuentaList;

  }

}
