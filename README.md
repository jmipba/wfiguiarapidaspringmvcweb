# WFIGuiaRapidaSpringMvcWEB

## Instrucciones para montar la guíar rápida en local ##

Para desplegar la aplicación WFIGuiaRapidaSpringMvcWEB, es necesario utilizar el servidor Tomcat proporcionado por IZFE (el cual tiene los jars necesarios) y seguir los siguientes pasos:

1. Para clonar el proyecto desde Eclipse, ir a la vista "Git Repositories" y hacer click sobre el botón "Clone a Git Repository and add the clone to this view".

2. En la ventana que se abre, copiar la URL "http://wasscv02.sare.gipuzkoa.net/WFI/WFIGuiaRapidaSpringMvcWEB.git" en el parámetro URI. Al llenar el parámetro URI, los parametros Host y Repository path se tienen que rellenar automáticamente. En la parte de autenticación, meter el usuario y contraseña en los parámetros User y Password. Pulsar Next.

3. En la siguiente ventana, seleccionar solo la rama "develop", deseleccionando todas las demás. Después pulsar Next.

4. En la última pantalla, verificar que en el parámetro Directory está el path donde se va a clonar el proyecto es correcto, y sino cambiarlo. También verificar que en el parámetro Initial branch está puesto "develop". Todo lo demás dejar como está y pulsar Finish.

5. Una vez clonado el proyecto en el equipo local, hay que importar el proyecto a Eclipse. Para ello, en el panel superior de Eclipse, hacer click sobre File > Import... Después, seleccionar "Existing Maven Projects" y hacer click en Next.

6. En la siguiente ventana, hacer click sobre Browse... y seleccionar el directorio raíz donde se ha clonado el proyecto en el paso 4. Después hacer click en Finish.

7. El siguiente paso es añadir los DataSources que necesita la aplicación a Tomcat. Para ello, hay que añadir la configuración de los datasources en los ficheros context.xml y server.xml. En el fichero context.xml hay que añadir lo siguiente dentro del elemento <Context>:
```
<ResourceLink name="jdbc/WFIGuiaRapidaDS" global="WFIGuiaRapidaDS" type="javax.sql.DataSource"/>
```

8. Y en el fichero server.xml hay que añadir lo siguiente dentro del elemento <GlobalNamingResources>:
```
<Resource auth="Container" driverClassName="com.ibm.db2.jcc.DB2Driver" name="WFIGuiaRapidaDS" password="WST" type="javax.sql.DataSource" url="jdbc:db2://desa.sare.gipuzkoa.net:446/DB2DHOST" username="WSTLOGTD"/>
```

9. Una vez añadido el DataSource que necesita, el siguiente paso es desplegarlo en Tomcat e ir a la URL "http://localhost:8080/WAS/HACI/WFIGuiaRapidaSpringMvcWEB/" en el navegador. Modificar "localhost" por la IP del servidor y "8080" por el puerto donde está Tomcat si es distinto.
  
10. Si se ejecuta desde fuera de la red de Izfe, se deberá tener permiso en la VPN para acceder a las siguientes máquinas:  
   - jmsdes01.sare.gipuzkoa.net:61616 (ActiveMQ de DESARROLLO tcp://jmsdes01.sare.gipuzkoa.net:61616)  
   - www9desa.sare.gipuzkoa.net (para recuperar estilos IBot --> www9desa.sare.gipuzkoa.net/estaticos/IBoT)  
   - desa.sare.gipuzkoa.net (para poder acceder al DB2 de DESARROLLO -puerto 446-)  
   